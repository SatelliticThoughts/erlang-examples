# erlang-examples

Erlang-examples contains code to demonstrate various things in Erlang.

## Set up

After cloning or downloading, most programs should be a single file that can be compiled and executed in erlang's REPL, unless otherwise stated.

```
erlc 'filename.erl' # compile file
erl -noshell -s 'filename' start -s init stop # run file 
```

## License
[MIT](https://choosealicense.com/licenses/mit/)

