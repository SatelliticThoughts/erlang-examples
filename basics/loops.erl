-module(loops).
-export([start/0]).

% Looping with recursion
loop(I) when I < 5 ->
    io:fwrite("~w~n", [I]),
    loop(I + 1);
loop(_) -> ok.

start() ->
    loop(0).
