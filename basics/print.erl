-module(print).
-export([start/0]).

start() ->
    % Hello World example
    io:fwrite("Hello World~n"),

    % variable example
    Text = "This is a test",
    io:fwrite("~s~n", [Text]),

    % multiple variables example
    Name = "luke",
    Age = 200102,
    io:fwrite("My name is ~s and i am ~w years old~n", [Name, Age]).
