-module(decisions).
-export([start/0]).

start() ->
    if
        true -> io:fwrite("This will show~n")
    end,

    if
        false -> io:fwrite("This will NOT show~n");
        true -> io:fwrite("This will show~n")
    end,

    if
        false -> io:fwrite("This will NOT show~n");
        true -> io:fwrite("This will show~n");
        true -> io:fwrite("This will NOT show~n")
    end,

    if
        true and true -> io:fwrite("This will show~n")
    end,

    if
        true and false -> io:fwrite("This will NOT show~n");
        true -> io:fwrite("required~n")
    end,

    if
        false and false -> io:fwrite("This will NOT show~n");
        true -> io:fwrite("required~n")
    end,

    if
        true or true -> io:fwrite("This will show~n")
    end,

    if
        true or false -> io:fwrite("This will show~n")
    end,

    if
        false or false -> io:fwrite("This will NOT show~n");
        true -> io:fwrite("required~n")
    end,

    X = 2,
    Y = 3,

    if
        X == Y -> io:fwrite("If X is equal to Y this will show~n");
        true -> io:fwrite("required~n")
    end,

    if
        X =/= Y -> io:fwrite("If X is not equal to Y this will show~n");
        true -> io:fwrite("required~n")
    end,

    if
        X > Y -> io:fwrite("If X is greater than Y this will show~n");
        true -> io:fwrite("required~n")
    end,

    if
        X < Y -> io:fwrite("If X is less than Y this will show#n");
        true -> io:fwrite("required~n")
    end.
