-module(variables).
-import(string,[concat/2]).
-import(string,[substr/3]).
-export([start/0]).

start() ->
    % Integers
    X = 14,
    Y = 3,

    io:fwrite("Integer Calculations, X = ~w, Y = ~w~n", [X, Y]),

    io:fwrite("X + Y = ~w~n",[X + Y]), % Addition
    io:fwrite("X - Y = ~w~n",[X - Y]), % Subtraction
    io:fwrite("X * Y = ~w~n",[X * Y]), % Multiplication
    io:fwrite("X / Y = ~w~n",[trunc(X / Y)]), % Division
    io:fwrite("X rem Y = ~w~n",[X rem Y]), % Modulus

    % Floating point
    A = 14.0021,
    B = 3.2,

    io:fwrite("~nFloating point calculations, A = ~w, B = ~w~n",[A, B]),

    io:fwrite("A + B = ~w~n",[A + B]), % Addition
    io:fwrite("A - B = ~w~n",[A - B]), % Subtraction
    io:fwrite("A * B = ~w~n",[A * B]), % Multiplication
    io:fwrite("A / B = ~w~n",[A / B]), % Division

    % Strings
    io:fwrite("~nString manipulation~n"),

    Hello = "hello",
    io:fwrite("~s~n",[Hello]),

    Hello2 = concat(Hello," world"),
    io:fwrite("~s~n", [Hello2]),

    io:fwrite("~s~n", [substr(Hello2, 3, 7)]), % substring

    % Lists
    Numbers = [5, 2, 1, 6, 7],
    io:fwrite("~nList Manipulation~n"),

    io:fwrite("~w~n",[Numbers]),
    io:fwrite("~w~n", [length(Numbers)]),

    Numbers2 = lists:append(Numbers, [9]),
    io:fwrite("~w~n",[Numbers2]),

    io:fwrite("~w~n", [lists:delete(6, Numbers2)]).
