-module(records).
-export([start/0]).

-record(person, {name = "", age}).

start() ->
    P = #person{name = "luke", age = 300},

    io:fwrite("Name: ~s, Age: ~w~n",[P#person.name, P#person.age]).
