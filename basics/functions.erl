-module(functions).
-export([start/0]).


sayHello() -> io:fwrite("Hello~n").

getWord() -> "dog".

doubleNumber(X) -> io:fwrite("~w~n", [X * 2]).

addNumbers(X, Y) -> X + Y.

start() ->
    sayHello(),
    io:fwrite("~s~n", [getWord()]),
    doubleNumber(5),
    io:fwrite("~w~n", [addNumbers(3, 2)]).
